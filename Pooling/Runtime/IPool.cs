﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DeepFramework.Pooling {
    public interface IPool {
        void PoolObjectReturned(PoolObject poolObject);
    }

    public interface IPool<in TPoolObjectType> where TPoolObjectType : IPoolObject {
        void PoolObjectReturned(TPoolObjectType poolObject);
    }
}