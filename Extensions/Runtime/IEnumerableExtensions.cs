﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DeepFramework.Extensions {
    public static class IEnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (null == source) throw new NullReferenceException(nameof(source));
            if (null == action) throw new NullReferenceException(nameof(action));

            foreach (var element in source)
            {
                action(element);
            }
        }
    }
}



