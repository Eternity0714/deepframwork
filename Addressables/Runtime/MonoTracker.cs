﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ResourceManagement.ResourceLocations;

public class MonoTracker : MonoBehaviour
{
    public delegate void DelegateDestroyed(MonoTracker tracker);
    public event DelegateDestroyed OnDestroyed;

    public IResourceLocation location { get; set; }

    void OnDestroy()
    {
        OnDestroyed?.Invoke(this);
    }
}
