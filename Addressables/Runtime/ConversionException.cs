﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepFramework.Addressables
{
    public class ConversionException : Exception
    {
        public ConversionException(string message) : base(message) { }
    }
}
