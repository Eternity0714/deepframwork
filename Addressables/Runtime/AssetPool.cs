﻿using DeepFramework.Pooling;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using Object = UnityEngine.Object;
using Ad = UnityEngine.AddressableAssets.Addressables;
using System.Linq;

namespace DeepFramework.Addressables {
    public abstract class AssetPool : IPool, IDisposable
    {
        public const int MinimumCapacityDefault = 5;

        static readonly Dictionary<IResourceLocation, AssetPool> AllPools = new Dictionary<IResourceLocation, AssetPool>();

        protected abstract Object loadedObject { get; }

        protected abstract ICollection collection { get; }

        protected bool _isReady;

        public virtual bool isReady => _isReady;

        public readonly IResourceLocation assetLocation;
        public readonly int initialCapacity;
        public readonly Transform objectParent;
        public int minimumCapacity;

        protected AssetPool(IResourceLocation assetLocation, int initialCapacity, Transform objectParent) {
            this.assetLocation = assetLocation;
            this.initialCapacity = initialCapacity;
            this.objectParent = objectParent;
            this.minimumCapacity = MinimumCapacityDefault;

            AllPools.Add(assetLocation, this);
        }

        public static bool TryGetPool(IResourceLocation location, out AssetPool assetPool) {
            return AllPools.TryGetValue(location, out assetPool);
        }

        public static bool TryGetPool<TComponent>(IResourceLocation location, out AssetPool<TComponent> assetPool) where TComponent : Component
        {
            if (AllPools.TryGetValue(location, out var p)) {
                assetPool = p as AssetPool<TComponent>;
                return true;
            }

            assetPool = null;
            return false;
        }

        protected abstract void AddToPool(int count);

        public abstract void PoolObjectReturned(PoolObject poolObject);

        #region IDisposable Support

        bool _disposed = false;
        protected bool disposed => _disposed; // 要检测冗余调用

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: 释放托管状态(托管对象)。
                    AllPools.Remove(assetLocation);
                    _isReady = false;
                }

                // TODO: 释放未托管的资源(未托管的对象)并在以下内容中替代终结器。
                // TODO: 将大型字段设置为 null。

                _disposed = true;
            }
        }

        // TODO: 仅当以上 Dispose(bool disposing) 拥有用于释放未托管资源的代码时才替代终结器。
        ~AssetPool()
        {
            // 请勿更改此代码。将清理代码放入以上 Dispose(bool disposing) 中。
            Dispose(false);
        }

        // 添加此代码以正确实现可处置模式。
        public void Dispose()
        {
            // 请勿更改此代码。将清理代码放入以上 Dispose(bool disposing) 中。
            Dispose(true);
            // TODO: 如果在以上内容中替代了终结器，则取消注释以下行。
            GC.SuppressFinalize(this);
        }

        #endregion

    }

    public class AssetPool<TComponent> : AssetPool where TComponent : Component {
        public delegate void DelegatePoolReady();
        public event DelegatePoolReady OnPoolReady;

        public delegate void DelegateObjectInstantiated(TComponent obj);
        public event DelegateObjectInstantiated OnObjectInstantiated;

        TComponent _loadedObject;
        protected override Object loadedObject => _loadedObject;

        List<Tuple<TComponent, PoolObject>> _list;
        protected override ICollection collection => _list;

        AsyncOperationHandle<TComponent> _loadHandle;
        public AsyncOperationHandle<TComponent> loadHandle => _loadHandle;

        Action<TComponent> _objectInstantiatedAction;

        protected AssetPool(IResourceLocation location, int initialCapacity, Transform objectParent, Action<TComponent> objectInstantiatedAction) : base(location, initialCapacity, objectParent) {
            _list = new List<Tuple<TComponent, PoolObject>>();
            _objectInstantiatedAction = objectInstantiatedAction;

            AssetManager.OnAssetUnloaded += OnObjectUnloaded;

            if (AssetManager.TryGetOrLoadObjectAsync(location, out _loadHandle))
            {
                OnObjectLoaded(_loadHandle.Result);
            }
            else {
                _loadHandle = Ad.ResourceManager.CreateChainOperation(_loadHandle, chainOp => {
                    OnObjectLoaded(chainOp.Result);
                    return chainOp;
                });
            }
        }

        void OnObjectLoaded(TComponent obj) {
            _loadedObject = obj;

            _isReady = true;

            AddToPoolSyncSafe(initialCapacity);

            OnPoolReady?.Invoke();
        }

        protected virtual void OnObjectUnloaded(IResourceLocation location) {
            if (location != this.assetLocation) return;
            Dispose();
        }

        protected override void AddToPool(int count)
        {
            if (isReady)
            {
                AddToPoolSyncSafe(count);
                return;
            }

            loadHandle.Completed += op =>
            {
                AddToPoolSyncSafe(count);
            };
        }

        void AddToPoolSyncSafe(int count) {
            var pos = objectParent ? objectParent.position : Vector3.zero;

            AssetManager.TryInstantiateMultiSync<TComponent>(assetLocation, count, pos, Quaternion.identity, objectParent, out var instanceList);

            foreach (var component in instanceList)
            {
                var po = component.gameObject.AddComponent<PoolObject>();
                po.myPool = this;
                po.ReturnToPool();

                var tuple = new Tuple<TComponent, PoolObject>(component, po);
                _list.Add(tuple);

                var monoTracker = component.GetComponent<MonoTracker>() ?? component.gameObject.AddComponent<MonoTracker>();
                monoTracker.OnDestroyed += tracker => { _list.Remove(tuple); };

                _objectInstantiatedAction?.Invoke(component);
                OnObjectInstantiated?.Invoke(component);
            }
        }

        public override void PoolObjectReturned(PoolObject poolObject)
        {
            if (objectParent)
                poolObject.transform.SetParent(objectParent);
        }

        public bool TryTake(Vector3 position, Quaternion rotation, Transform parent, out AsyncOperationHandle<TComponent> handle) {
            if (isReady) {
                handle = Ad.ResourceManager.CreateCompletedOperation(TakeSync(position, rotation, parent), string.Empty);
                return true;
            }

            handle = Ad.ResourceManager.CreateChainOperation(_loadHandle, chainOp => {
                var result = TakeSync(position, rotation, parent);
                var completedOp = Ad.ResourceManager.CreateCompletedOperation(result, string.Empty);
                return completedOp;
            });
            return false;
        }

        public TComponent TakeSync(Vector3 position, Quaternion rotation, Transform parent = null) {
            string errString = $"{GetType()} Error:";

            if (!isReady) {
                Debug.LogError($"{errString} not ready.");
                return null;
            }

            var inPoolCount = _list.Count(x => x.Item2.inPool);

            if (inPoolCount <= minimumCapacity)
            {
                AddToPool(5);
                inPoolCount = _list.Count(x => x.Item2.inPool);
            }

            if (inPoolCount == 0) {
                Debug.LogError($"{errString} Empty. Consider setting a higher {nameof(minimumCapacity)}");
                return null;
            }

            var tuple = _list.FirstOrDefault(x => x.Item2.inPool);
            var transform = tuple.Item1.transform;
            transform.position = position;
            transform.rotation = rotation;
            if (parent)
                transform.parent = parent;
            tuple.Item2.AwakeFromPool();

            return tuple.Item1;
        }

        public static AssetPool<TComponent> GetOrCreate(IResourceLocation location, int initialCapacity = 10, Transform objectsParent = null, Action<TComponent> objectInstantiationAction = null) {
            if (TryGetPool<TComponent>(location, out var existingPool)) {
                return existingPool;
            }

            var pool = new AssetPool<TComponent>(location, initialCapacity, objectsParent, objectInstantiationAction);
            return pool;
        }

        public static AssetPool<TComponent> GetOrCreate(IResourceLocation location, Action<TComponent> objectInstantiationAction) {
            return GetOrCreate(location, 10, null, objectInstantiationAction);
        }

        #region IDisposable Support
        protected override void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing) {
                AssetManager.OnAssetUnloaded -= OnObjectUnloaded;
                _list.Clear();
            }
            base.Dispose(disposing);
        }
        #endregion
    }


}
